int activityIndicatorLEDPin = 13;
int powerIndicatorLEDPin = 6;
int pwmPin = 11;
uint8_t outputState = LOW;
unsigned long lastCheckTime = 0;
 
constexpr int potentiometerPin = A0;
constexpr double potMultiplier = 1.50;
constexpr double potThresholdLow = 170;
constexpr double potThresholdHigh = 1000;

void setup()
{
  Serial.begin(9600);
  while (!Serial) {}

  //tweakPWMFrequency();

  pinMode(activityIndicatorLEDPin, OUTPUT);
  pinMode(powerIndicatorLEDPin, OUTPUT);
  pinMode(pwmPin, OUTPUT);
  pinMode(potentiometerPin, INPUT);
  lastCheckTime = millis();

  digitalWrite(powerIndicatorLEDPin, HIGH);
}


void loop()
{
  double potValue = analogRead(potentiometerPin) * potMultiplier;
  potValue = (potValue < potThresholdLow) ? 0 : (potValue > potThresholdHigh ? 1023 : potValue);
  const int pwmOutput = (1023 - int(ceil(potValue))) / 4; // 1023 (max value for analog read) / 4 -> 255 (max value for analog write)
  analogWrite(pwmPin, pwmOutput);

  auto time = millis();
  if ((time - lastCheckTime) > 500)
  {
    outputState = !outputState;
    digitalWrite(activityIndicatorLEDPin, outputState);
    lastCheckTime = time;
  }

  delay(50);
}

// The Arduino framework assigns the fixed PWM frequency of 490 Hz for PWM on pin 11.
// This function increases the frequency to 980 Hz by changing the ATmega328 register values which control timer #2.
// https://nerdytechy.com/how-to-change-the-pwm-frequency-of-arduino/
void tweakPWMFrequency()
{
  TCCR2B = 0b00000011; // x32
  TCCR2A = 0b00000001; // phase correct
}
