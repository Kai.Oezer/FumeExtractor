# Fume Extractor

This repository contains the hardware design files and microcontroller program code for building a solder fume extractor. Building this device requires an off-the-shelf 12 cm desktop PC fan with 4 mm screw holes and an Arduino Pro Mini (or compatible) microcontroller board for controlling the speed of the fan. A much simpler, single-speed ventilation device can be built by using just a mechanical switch. Using a microcontroller, however, we can easily modify the knob-position-to-speed mapping and the behavior of the LED. Furthermore, the circuit serves as an example of applying a PWM signal to the electronic power switch consisting of a JFET and a Darlington BJT, which could be useful in other applications.

<div style="display: inline-block">
<a href="images/img4.jpg">
	<img src="images/img4.jpg" alt="front view" style="height:200px">
</a>
<a href="images/img2.jpg">
	<img src="images/img2.jpg" alt="left side view" style="height:200px">
</a>
<a href="images/img3.jpg">
	<img src="images/img3.jpg" alt="back view" style="height:200px">
</a>
<a href="images/img5.jpg">
	<img src="images/img5.jpg" alt="with battery attached" style="height:200px">
</a>
<a href="images/img1.jpg">
	<img src="images/img1.jpg" alt="disassembled case with circuit board" style="height:200px">
</a>
</div>

## Bill of materials
* one 12 cm desktop PC fan with input voltage range 5 to 12 V
* one Arduino Mini Pro or compatible microcontroller
* one BS170 N-channel JFET
* one TIP120 NPN Darlington BJT transistor
* one 47 kOhm, one 220 Ohm and one 1 kOhm resistor
* one 100 kOhm potentiometer, circa 24 mm tall, 6 mm knob diameter, plus cap
* one 3 mm LED with circa 200 mcd intensity, V<sub>f,max</sub> = 2 V, I<sub>f,typical</sub> = 20 mA
* one perfboard 50 mm x 50 mm x 1.6 mm with 0.1 inch pad spacing (19 x 19 pads)
* two M4 screw with nut to attach the legs to the fan
* eight M2.5 screws (6 to 10 mm long) to attach the control unit

## Required hardware tools
* 3D printer
* Soldering equipment

## Required software tools
* [Arduino IDE](https://docs.arduino.cc/software/ide-v2)
* slicing software for your 3D printer

## Software tools for development
* [FreeCAD](https://www.freecadweb.org)
* [KiCad](https://www.kicad.org)

## Circuit board schematic and PCB layout

<a href="images/circuit.png">
	<img src="images/circuit.png" alt="circuit schematic" style="height:300px">
</a>

<a href="images/pcb.png">
	<img src="images/pcb.png" alt="circuit PCB layout" style="height:300px">
</a>
